/* eslint-env node */
'use strict';

const path = require('path');
// const fs = require('fs');

const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const basePath = 'web/webroot/_ui/responsive/theme-fn/app';

// cache remove
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/babel-loader/'), {recursive: true});
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/terser-webpack-plugin/'), {recursive: true});
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/vue-loader/'), {recursive: true});
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/'), {recursive: true});

module.exports = (env, arg) => {
	// const cacheIdentifier = (+ new Date()).toString();
	
	const config = {
		context: path.resolve(__dirname, `${basePath}/src2`),
		
		// watch: true,
		watchOptions: {
			ignored: ['node_modules/**/*'],
		},
		mode: arg.mode,
		plugins: [
			new VueLoaderPlugin(),
			new FriendlyErrorsWebpackPlugin(),
			new CaseSensitivePathsPlugin(),
			new webpack.DefinePlugin({
				'process.env.NODE_ENV': JSON.stringify(arg.mode),
			}),
		],
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname,  `${basePath}/dist`)
		},
		// devtool: 'eval-source-map',
		// devServer: {
		// 	contentBase: './dist',
		// 	compress: true,
		// 	port: 9005,
		// 	https: true,
		// 	hot: true,
		// },
		module: {
			noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/,
			rules: [
				{
					test: /\.vue$/,
					use: [
						{
							loader: 'cache-loader',
							options: {
								cacheDirectory: path.resolve(__dirname, 'node_modules/.cache/vue-loader'),
								// cacheIdentifier: cacheIdentifier,
							}
						},
						{
							loader: 'vue-loader',
							options: {
								hotReload: false,
								// cacheDirectory: path.resolve(__dirname, 'node_modules/.cache/vue-loader'),
								productionMode: JSON.stringify(arg.mode),
								compilerOptions: {
									whitespace: 'condense',
								}
							}
						}
					]
				},
				{
					test: /\.m?js$/,
					use:
						{
							loader: 'babel-loader',
							options: {
								cacheDirectory: true,
								cacheCompression: false,
								// cacheIdentifier: cacheIdentifier,
								exclude: (file) => (
										/node_modules/.test(file) &&
										!/\.vue\.js/.test(file)
								),
								presets: [
									['@babel/preset-env', {
										useBuiltIns: false,
										corejs: {
											version: 2,
											proposals: false
										}
										// include: [`es6.object.assign`],
										// debug: true,
									}]
								],
							}
						}
				},
			]
		},
		// optimization: {
			// minimize: true,
			// minimizer: [new TerserPlugin()],
			// runtimeChunk: 'single',
			// splitChunks: {
			// 	cacheGroups: {
			// 		vendor: {
			// 			test: /[\\/]node_modules[\\/]/,
			// 			name: 'vendors',
			// 			chunks: 'all'
			// 		}
			// 	}
			// }
		// },
		externals: {
			lodash: '_',
			vue: 'Vue',
			jquery: '$',
		},
		resolve: {
			alias: {
				'@': path.resolve(__dirname, `${basePath}/src2`),
				'&': path.resolve(__dirname, `${basePath}/src2/comp`), 
				vue$: 'vue/dist/vue.esm.js'
			},
			extensions: ['.mjs', '.js', '.vue', 'json']
		},
		entry: {
			order_detail: './view/order_detail.js',
			cart: './view/cart.js',
			checkout_delivery_address: './view/checkout_delivery_address.js',
			pdp: './view/pdp.js',
			// email_deny: './view/email_deny.js',
		}
	};
	
	if(arg.mode !== 'production'){
		config.watch = true;
		config.devtool = 'eval-source-map';
	}
		
	return config;
};