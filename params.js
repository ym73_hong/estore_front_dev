'use strict';

module.exports = () => {
	const minimist = require('minimist');
	
	let options = minimist(process.argv.slice(2), [
		{
			string: 'webroot',
			default: { webroot: undefined }
		},
		{
			string: 'site',
			default: { site: undefined }
		},
		{
			string: 'mode',
			default: { mode: undefined }
		},
		{
			string: 'html',
			default: {html: undefined}
		}
	]);
	
	return {
		webroot: `${options.webroot}/hybris/bin/custom/toko/tokostorefront/web/webroot`,
		site: options.site,
		mode: options.mode,
		html: options.html
	};
};