'use strict';

const params = require('./params')();
const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
const mail = async () => {
  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
		host: 'localhost',
		port: 25,
		secure: false,
		localAddress: '127.0.0.1',
		debug: true,
		// auth: {
		// 	user: '',
		// 	pass: '',
		// },
		// tls: {
		// 	rejectUnauthorized: true
		// }
  });

	// verify connection configuration
transporter.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages");
  }
});

  // // send mail with defined transport object
  // const info = await transporter.sendMail({
  //   from: 'ym73.hong@partner.samsung.com', // sender address
  //   to: "ym73.hong@partner.samsung.com", // list of receivers
  //   subject: "Hello ✔", // Subject line
  //   text: "Hello world?", // plain text body
  //   html: "<b>Hello world?</b>", // html body
	// });
	
  // console.log("Message sent: %s", info.messageId);
};

mail().catch(console.error);

// // async..await is not allowed in global scope, must use a wrapper

//   // create reusable transporter object using the default SMTP transport
//   const transporter = nodemailer.createTransport({
// 		host: '127.0.0.1',
// 		port: 25,
// 		secure: false,
// 		debug: true,
// 		auth: {
// 			user: "",
// 			pass: ""
// 		}
//   });
//   // send mail with defined transport object
//   transporter.sendMail({
//     from: 'ym73.hong@partner.samsung.com', // sender address
//     to: "ym73.hong@partner.samsung.com", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
// 	}, (err) => {
// 		console.log(err);
// 	});
	
// 	console.log('eeee');
// //   console.log("Message sent: %s", info.messageId);

// // mail().catch(console.error);



// // async..await is not allowed in global scope, must use a wrapper
// async function main() {
//   // Generate test SMTP service account from ethereal.email
//   // Only needed if you don't have a real mail account for testing
//   let testAccount = await nodemailer.createTestAccount();

//   // create reusable transporter object using the default SMTP transport
//   let transporter = nodemailer.createTransport({
//     host: "smtp.ethereal.email",
//     port: 587,
//     secure: false, // true for 465, false for other ports
//     auth: {
//       user: testAccount.user, // generated ethereal user
//       pass: testAccount.pass, // generated ethereal password
// 		},
// 		proxy: 'http://70.10.15.10:8080'
//   });

//   // send mail with defined transport object
//   let info = await transporter.sendMail({
//     from: '"Fred Foo 👻" <foo@example.com>', // sender address
//     to: "bar@example.com, baz@example.com", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>", // html body
//   });

//   console.log("Message sent: %s", info.messageId);
//   // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//   // Preview only available when sending through an Ethereal account
//   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//   // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
// }

// main().catch(console.error);