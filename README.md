**estore_front_dev** 는 fn 과 b2c 의 tokostorefront 프론트 리소스 빌드 도구 입니다.

본 내용은 windows 10 64bit 를 기준으로 작성되었습니다. 
Mac OS / Linux 사용자는 문의 주시기 바랍니다.



# 설치

## 브랜치 복제

반드시 **소스가 있는 동일 디스크에 위치해야 합니다**. 
일반적으로 소스가 위치한 폴더에 git clone 을 권장 합니다. 

샘플 디렉토리 구조는 다음과 같습니다.

![1531887172470](assets/1531887172470.png)



## node.js LTS 설치

https://nodejs.org/en/ 에서 최신의 LTS 버전을 받아서 설치 합니다.
만일,  LTS 보다 높은 버전이 설치된 경우엔, 반드시 제어판에서 node.js 삭제후 LTS 버전을 설치합니다.



## npm update

npm 은 개별적인 업그레이드 주기를 가집니다. 꼭 npm 업그레이드를 합니다.

```bash
npm install -g npm@latest
```



## 패키지 설치

estore_front_dev 폴더에서 cmd 창을 열고 순서대로 설치 합니다.

> **주의 !!**
>
> Git Bash 창에서 실행하지 마세요 



### gulp CLI

```bash
npm install gulp-cli -g
```



### 로컬 패키지 설치

```bash
npm install
```





# 설정

**utf-8**  로 저장할 수 있는 텍스트 에디터로 package.json 을 엽니다.

scripts 항목에서 --webroot 파라메터를 알맞게 지정합니다.
(반드시 unix 스타일의 상대 경로로 지정해야 합니다. C:\ 같은 windows 스타일의 경로지정은 안됩니다)

```json
  "scripts": {
    "gulp_sec_prd": "gulp --gulpfile=gulpfile.js -L build --webroot=../../3.Workspace/toko6-kr --site=sec --mode=prd",
    "gulp_sec_dev": "gulp --gulpfile=gulpfile.js -L watch --webroot=../../3.Workspace/toko6-kr --site=sec --mode=dev",
    "gulp_fn_prd": "gulp --gulpfile=gulpfile.js -L build --webroot=../../FN.Workspace/toko6-kr --site=fn --mode=prd",
    "gulp_fn_dev": "gulp --gulpfile=gulpfile.js -L watch --webroot=../../FN.Workspace/toko6-kr --site=fn --mode=dev"
  },
```





# Gulp 실행

Grunt 는 사용하지 않습니다. 그러나, 서버는 여전히 Grunt 를 이용하고 있으므로  Grunt.js 를 지우거나 변경해서는 안됩니다.

### sec

```bash
npm run gulp_sec_dev
```

### fn

```bash
npm run gulp_fn_dev
```



fn, sec 각각 .bat 파일로 만들어서 사용하길 권장합니다. 
아래는 현재 사용중인 bat 파일 입니다. 자신의 환경에 맞게 경로 등을 수정하시기 바랍니다.

```powershell
chcp 65001
C:
cd C:\toko\10_front_dev\estore_front_dev
call npm run gulp_sec_prd
```

> **주의 !!** 
>
> .bat 파일은 반드시 메모장에서 ANSI 타입으로 저장해야 합니다.


