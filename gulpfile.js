'use strict';

const { series, parallel } = require('gulp');
const params = require('./params')();
// const beep = require('./gulp/beep');
const sass = require('./gulp/sass')(params);
const less = require('./gulp/less')(params);
const app = require('./gulp/app')(params);
// const common = require('./gulp/common')(params);
// const site = require('./gulp/site')(params);
const jsAll = require('./gulp/all')(params);
const mail_form = require('./gulp/mail_form')(params);
const lib = require('./gulp/lib')(params, false);
const lib_with_angular = require('./gulp/lib')(params, true);

// task
const task_build = series(
	parallel(
		sass.build,
		less.build,
		app.build,
		// common.build,
		// site.build,
		jsAll.build,
		lib.build,
		lib_with_angular.build
	),
	mail_form.build
);

const task_watch_build = parallel(
	sass.build,
	less.build,
	app.build,
	// common.build,
	// site.build,
	jsAll.build,
	lib.build,
	lib_with_angular.build
);

// watch 개별 선언
const task_watch = (cb) => {
  less.watch();
	sass.watch();
	app.watch();
	// common.watch();
	// site.watch();
	jsAll.watch(),
	lib.watch();
	lib_with_angular.watch();
	mail_form.watch();
	
	cb();
};

// // watch 통합 선언.
// // 복합적인 오류를 처리할때 유용 ...
// const watch_all = () => {
//   const watcher = watch([].concat(sass.source, less.source, app.source), options.watch, task);
//   watcher.on('error', (path, state) => {
//     beep();
//   });
// }
// exports.watch_all = series(task, watch_all);

exports.build = task_build;

exports.watch = series(task_watch_build, mail_form.build, task_watch);

exports.mailform = series(mail_form.build);

