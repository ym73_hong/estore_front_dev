/* eslint-env node */
const path = require('path');
// const fs = require('fs');

const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const basePath = 'web/webroot/_ui/responsive/theme-sec/app';

// cache remove
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/babel-loader/'), {recursive: true});
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/terser-webpack-plugin/'), {recursive: true});
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/vue-loader/'), {recursive: true});
// fs.rmdirSync(path.resolve(__dirname, 'node_modules/.cache/'), {recursive: true});

module.exports = (env, arg) => {
	// const cacheIdentifier = (+ new Date()).toString();
	
	const config = {
		context: path.resolve(__dirname, `${basePath}/src2`),
		
		// watch: true,
		watchOptions: {
			ignored: ['node_modules/**/*'],
		},
		mode: arg.mode,
		plugins: [
			new VueLoaderPlugin(),
			new FriendlyErrorsWebpackPlugin(),
			new CaseSensitivePathsPlugin(),
			new webpack.DefinePlugin({
				'process.env.NODE_ENV': JSON.stringify(arg.mode),
			}),
		],
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname,  `${basePath}/dist`)
		},
		// devtool: 'eval-source-map',
		// devServer: {
		// 	contentBase: './dist',
		// 	compress: true,
		// 	port: 9005,
		// 	https: true,
		// 	hot: true,
		// },
		module: {
			noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/,
			rules: [
				{
					test: /\.vue$/,
					use: [
						{
							loader: 'cache-loader',
							options: {
								cacheDirectory: path.resolve(__dirname, 'node_modules/.cache/vue-loader'),
								// cacheIdentifier: cacheIdentifier,
							}
						},
						{
							loader: 'vue-loader',
							options: {
								hotReload: false,
								// cacheDirectory: path.resolve(__dirname, 'node_modules/.cache/vue-loader'),
								productionMode: JSON.stringify(arg.mode),
								compilerOptions: {
									whitespace: 'condense',
								}
							}
						}
					]
				},
				{
					test: /\.m?js$/,
					use:
						{
							loader: 'babel-loader',
							options: {
								cacheDirectory: true,
								cacheCompression: false,
								// cacheIdentifier: cacheIdentifier,
								presets: [
									['@babel/preset-env', {
										useBuiltIns: 'false',
										corejs: 3,
										// include: [`es6.object.assign`],
										// debug: true,
									}]
								],
							}
						}
				},
			]
		},
		// optimization: {
			// minimize: true,
			// minimizer: [new TerserPlugin()],
			// runtimeChunk: 'single',
			// splitChunks: {
			// 	cacheGroups: {
			// 		vendor: {
			// 			test: /[\\/]node_modules[\\/]/,
			// 			name: 'vendors',
			// 			chunks: 'all'
			// 		}
			// 	}
			// }
		// },
		externals: {
			lodash: '_',
			vue: 'Vue',
			jquery: '$',
		},
		resolve: {
			alias: {
				'@': path.resolve(__dirname, `${basePath}/src2`),
				vue$: 'vue/dist/vue.esm.js'
			},
			extensions: ['.mjs', '.js', '.vue', 'json']
		},
		entry: {
			app_gnb: './app_gnb.js',
			app_gnb_simple: './app_gnb_simple.js',
			app_nav: './app_nav.js',
			app_footer: './app_footer.js',
			app_cart: './app_cart.js',
			app_checkout_payment: './app_checkout_payment.js',
			checkout_address: './view/checkout_address.js',
			order_confirmation: './view/order_confirmation.js',
			login_checkout: './view/login_checkout.js',
			// profile: path.join(basePath, 'src2/view/profile.js'),
		}
	};
	
	if(arg.mode !== 'production'){
		config.watch = true;
		config.devtool = 'eval-source-map';
	}
		
	return config;
};