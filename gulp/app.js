'use strict';

// 모두 webpack 으로 전환하기 전까지만 사용 !!
module.exports = (params) => {
	const {src, dest, watch} = require('gulp');
	const gulp_uglyfly = require('gulp-uglify');
	const gulp_sourcemaps = require('gulp-sourcemaps');
	const gulp_if = require('gulp-if');
	
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const basePath = `${params.webroot}/_ui/responsive/theme-${params.site}/app`; 
	const source = [`${basePath}/src/**/*.js`];
	
	const build = () => {
		const condition = () => params.mode === 'dev' ? true : false;
		
		return src(source, options.src)
			.pipe(gulp_if(condition, gulp_sourcemaps.init()))
			.pipe(gulp_uglyfly(options.uglyfly.app))
			.pipe(gulp_if(condition, gulp_sourcemaps.write('.', {includeContent: true, sourceRoot: '../src'})))
			.pipe(dest('dist', {cwd: basePath}))
			.on('end', () => {
				msg.success('App build');
			});
	};
 
	return {
		build: build,
		watch(){
			const watcher = watch(source, options.watch, build);
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};

