'use strict';

module.exports = (webroot, site, mode, withAngular) => {
	const _path = require('path');
	const lib_list = require(`./lib_list_${site}`)();
	const _list = withAngular ? lib_list : lib_list.filter(item => !item.isAngular );
	
	if(mode === 'dev'){
		_list.splice(0,0, {
			name: 'vue.js',
			isMinified: true,
			isAngular: false,
		});
	} else {
		_list.splice(0,0, {
			name: 'vue.min.js',
			isMinified: true,
			isAngular: false,
		});
	}
	
	return {
		getList() {
			return _list.map((item) => `${webroot}/_ui/responsive/theme-${site}/lib/src/${item.name}`);
		},
		getIsMinified(path) {
			let retVal = false;
			const baseName = _path.basename(path);
			
			for(let i = 0 ; i < _list.length ; i++){
				if(_list[i].name === baseName){
					retVal = _list[i].isMinified;
					break;
				}
			}
			
			return retVal;
		}
	};
};