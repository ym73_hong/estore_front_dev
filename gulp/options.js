'use strict';

module.exports = (() => {
	return {
		sass: {
			outputStyle: 'compressed',
			// precision: 8	//bootstrap 3.x 호환을 위해 설정함, but Dart SASS 사용시 10자리로 고정되므로 필요 없음
		},
		
		autoprefixer: {
			sass: {
				cascade: false,
				grid: 'no-autoplace',
				overrideBrowserslist: [
					'last 1 chrome version',
					'last 1 android version',
					'last 1 and_chr version',
					'last 1 edge version',
					'last 1 Firefox version',
					'last 1 ios_saf version',
					'last 1 Safari version',
					'ie 10-11',
				]
			},
			less: {
				cascade: true,
				overrideBrowserslist: [
					'last 1 chrome version',
					'last 1 android version',
					'last 1 and_chr version',
					'last 1 edge version',
					'last 1 Firefox version',
					'last 1 ios_saf version',
					'last 1 Safari version',
					'ie 10-11',
				]
			}
		},
		
		watch: {
			ignoreInitial: true,
			events: 'all',
			alwaysStat: false,
			useFsEvents: true,
			awaitWriteFinish: true,
		},
		
		src :{
			nosort: true,
			allowEmpty: true,
			strict: true,
		},
		
		uglyfly : {
			app : {
				parse: {},
				mangle: {},
				compress: {
					drop_debugger: false
				},
				warnings: true
			},
			
			// minification 안된 것
			lib: {
				parse: {},
				mangle: false,
				compress: true,
				warnings: true,
			},
			
			// minification 된 것
			lib_no_compress: {
				parse: {},
				mangle: false,
				compress: false,
				warnings: true,
			},
			
			// AI 제작 소스는 mangle 하지 않음
			AI: {
				parse: {},
				mangle: false,
				compress: {},
				warnings: true
			},
			
			site_dev: {
				// parse: {},
				mangle: false,
				// compress: {
				// 	drop_debugger: false
				// },
				compress: false,
				warnings: true
			},
			site_prd: {
				// parse: {},
				// mangle: false,
				// compress: {},
				warnings: true
			},
			
			// test: {
			// 	// parse: {},
			// 	mangle: false,
			// 	compress: false,
			// 	warnings: true,
			// }
		}		
	};
})();