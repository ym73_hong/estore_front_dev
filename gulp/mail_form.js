'use strict';

module.exports = (params) => {
	const {src, dest, watch} = require('gulp');
	const gulp_inlineCss = require('gulp-inline-css');

	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const basePath = `${params.webroot}/_ui/responsive/theme-${params.site}/mail_templete`;
	const src_html = `${basePath}/src/**/*.html`;
	const src_css = `${params.webroot}/_ui/responsive/theme-${params.site}/css/mail.css.map`;
	
	const build = () => {
		const source = [src_html];
		
		return src(source, options.src)
			.pipe(gulp_inlineCss({
				applyStyleTags: true,
				applyLinkTags: true,
				removeStyleTags: true,
				removeLinkTags: true,
				removeHtmlSelectors: true,
				// url: ' ',
				codeBlocks: {
					velocity: {start: '${', end: '}'}
				}
			}))
			.pipe(dest('dist', {cwd: basePath}))
			.on('end', () => {
				msg.success('Mail Form build');
			});
	};
	
	return {
		build: build,
		watch() {
			const watcher = watch([src_html, src_css], options.watch, build);
			
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};
