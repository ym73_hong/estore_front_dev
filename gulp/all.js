'use strict';

// 모두 webpack 으로 전환하기 전까지만 사용 !!
module.exports = (params) => {
	const path = require('path');
	const {src, dest, watch} = require('gulp');
	const gulp_uglyfly = require('gulp-uglify');
	const gulp_sourcemaps = require('gulp-sourcemaps');
	const gulp_concat = require('gulp-concat');
	const gulp_if = require('gulp-if');
	const print = require('gulp-print').default;
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const base = `${params.webroot}/_ui`;
	const source = require(`./common_list_${params.site}`)()
		.common
		.map((currentValue) => {
			return `${base}/${currentValue}`;
		})
		.concat([`${base}/responsive/theme-${params.site}/js/**/*.js`, `${base}/responsive/common/js/_autoload.js`]);
	
	const build = () => {
		const distFile = `${params.site}.js`;
		const dist = `${base}/responsive/theme-${params.site}`;
		const condition = () => params.mode === 'dev' ? true : false;
		
		return src(source, options.src)
			.pipe(gulp_if(condition, gulp_sourcemaps.init()))
			.pipe(gulp_if(condition, gulp_uglyfly(options.uglyfly.site_prd), gulp_concat(distFile)))
			
			.pipe(gulp_if(condition, gulp_sourcemaps.mapSources(function(sourcePath, file) {
				// console.log(`${file.base}/${sourcePath}`);
				return path.join(path.relative(dist, file.base), sourcePath);
			})))
			.pipe(gulp_if(condition, gulp_concat(distFile), gulp_uglyfly(options.uglyfly.site_prd)))
			.pipe(gulp_if(condition, gulp_sourcemaps.write('.', {includeContent: true})))
			.pipe(dest('.', {cwd: dist}))
			.on('end', () => {
				msg.success(`${distFile} build`);
			});
	};
 
	return {
		build: build,
		watch() {
			const watcher = watch(source, options.watch, build);
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};