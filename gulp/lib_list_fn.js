'use strict';

module.exports = () => [
		{
			name: 'vue_extend.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'lodash.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'angular.js',
			isMinified: false,
			isAngular: true,
		},
		{
			name: 'bootstrap.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'enquire.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'Imager.min.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.blockUI-2.66.js',
			isMinified: false,
			isAngular: false,
		},
		// {
		// 	name: 'jquery.colorbox-min.js',
		// 	isMinified: true,
		// 	isAngular: false,
		// },
		{
			name: 'jquery.bootstrap-select.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.form.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'jquery.hoverIntent.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.imageloader.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.pstrength.custom-1.2.0.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.syncheight.custom.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.tabs.custom.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery-ui-1.11.2.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'jquery.zoom.custom.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.picturefill.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'owl.carousel.custom.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'pushmenu.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'js.cookie.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.tmpl-1.0.0pre.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'jquery.currencies.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'jquery-sagify.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'jquery.scrollanimate.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'ng-infinite-scroll.min.js',
			isMinified: true,
			isAngular: true,
		},
		{
			name: 'elif.js',
			isMinified: false,
			isAngular: true,
		},
		{
			name: 'angular-animate.js',
			isMinified: false,
			isAngular: true,
		},
		{
			name: 'moment.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'bootstrap-accessibility.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'dropzone.js',
			isMinified: false,
			isAngular: false,
		},
		{
			name: 'slick.min.js',
			isMinified: true,
			isAngular: false,
		},
		{
			name: 'jsrender.min.js',
			isMinified: true,
			isAngular: false,
		},
	];