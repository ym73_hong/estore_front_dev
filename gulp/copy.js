'use strict';

module.exports = (params) => {
	const _path = require('path');
	const fs = require('fs');
	const {watch} = require('gulp');
	// const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	// const src_base = `${webroot}/WEB-INF/_ui-src`;
	// const src_build = [`${src_base}/responsive/themes/${site}/less/style.less`];
	const src_watch = [
		`${params.webroot}/WEB-INF/_ui-src/responsive/themes/${params.site}/js/*.js`,
	];
	const dist_watch = `${params.webroot}/_ui/responsive/theme-${params.site}/js`;

	// const build = () => {
	// 	return src(src_build ,options.src)

	// 		.pipe(dest('.', {cwd: dist}))
	// 		.on('end', () => {
	// 			msg.success('LESS build');
	// 		});
	// };
	const watch_options = {
		ignoreInitial: true,
		// events: 'all',
		alwaysStat: true,
		useFsEvents: true,
		awaitWriteFinish: true,
	};
		
	return {
		watch() {
			const watcher = watch(src_watch, watch_options);
			watcher.on('add', (path) => {
				let basename = _path.basename(path);
				
				msg.log('add', path );
				fs.copyFileSync(path, `${dist_watch}/${basename}`);
				msg.success(`${basename} copy`);
			});
			watcher.on('change', (path) => {
				let basename = _path.basename(path);
				
				msg.log('change', path);
				fs.copyFileSync(path, `${dist_watch}/${basename}`);
				msg.success(`${basename} copy`);
			});
			watcher.on('unlink', (path) => {
				let basename = _path.basename(path);
				
				msg.log('unlink', path);
				fs.unlinkSync(`${dist_watch}/${basename}`);
				msg.success(`${basename} unlink`);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};