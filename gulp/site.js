'use strict';

// 모두 webpack 으로 전환하기 전까지만 사용 !!
module.exports = (params) => {
	const {src, dest, watch} = require('gulp');
	const gulp_uglyfly = require('gulp-uglify');
	const gulp_sourcemaps = require('gulp-sourcemaps');
	const gulp_concat = require('gulp-concat');
	const gulp_if = require('gulp-if');
	// const print = require('gulp-print').default;
	
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const base = `${params.webroot}/_ui/responsive/theme-${params.site}`; 
	const source = [`${base}/js/**/*.js`];
	
	const build = () => {
		const distFile = `${params.site}.js`;
		const condition = () => params.mode === 'dev' ? true : false;
		
		return src(source, options.src)
		// .pipe(print())
			
			.pipe(gulp_if(condition, gulp_sourcemaps.init()))
			
			.pipe(gulp_if(condition, gulp_uglyfly(options.uglyfly.site_prd), gulp_concat(distFile)))
			.pipe(gulp_if(condition, gulp_concat(distFile), gulp_uglyfly(options.uglyfly.site_prd)))
			// .pipe(gulp_uglyfly(options.uglyfly.site_prd))
			// .pipe(gulp_concat(distFile))
			
			.pipe(gulp_if(condition, gulp_sourcemaps.write('.', {includeContent: true, sourceRoot: 'js'})))
			.pipe(dest('.', {cwd: base}))
			.on('end', () => {
				msg.success(`${distFile} build`);
			});
	};
 
	return {
		build: build,
		watch(){
			const watcher = watch(source, options.watch, build);
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};

