'use strict';

module.exports = (params) => {
	const {src, dest, watch} = require('gulp');
	const gulp_sass = require('gulp-sass');
	const gulp_postcss = require('gulp-postcss');
	const autoprefixer = require('autoprefixer');
	const gulp_sourcemaps = require('gulp-sourcemaps');
	const gulp_if = require('gulp-if');
	
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const basePath = `${params.webroot}/_ui/responsive/theme-${params.site}`; 
	
	gulp_sass.compiler = require('sass');
	options.sass.fiber = require('fibers');
	
	const build = () => {
		const source = [`${basePath}/sass/style_new.scss`, `${basePath}/sass/mail.scss`];
		// const source = [`${basePath}/sass/netfunnel.scss`, `${basePath}/sass/style_new.scss`, `${basePath}/sass/mail.scss`];
		const condition = () => params.mode === 'dev' ? true : false;
		
		return src(source, options.src)
			.pipe(gulp_if(condition, gulp_sourcemaps.init()))
			.pipe(gulp_sass(options.sass))
			.pipe(gulp_postcss([autoprefixer(options.autoprefixer.sass)]))
			.pipe(gulp_if(condition,
				gulp_sourcemaps.write('.', {includeContent: true, sourceRoot: '../sass'}))
			)
			.pipe(dest('css', {cwd: basePath}))
			.on('end', () => {
				msg.success('SASS build');
			});
	};
	
	return {
		build: build,
		watch() {
			const source = [`${basePath}/sass/**/*.scss`];
			const watcher = watch(source, options.watch, build);
			
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};
