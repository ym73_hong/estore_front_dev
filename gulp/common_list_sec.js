'use strict';

module.exports = function(){
	const common_list = [
		'shared/js/analyticsmediator.js',

		'responsive/common/js/acc.global.js',
		'responsive/common/js/acc.address.js',
		'responsive/common/js/acc.carousel.js',
		'responsive/common/js/acc.cart.js',
		'responsive/common/js/acc.cartitem.js',
		'responsive/common/js/acc.checkout.js',
		// 'responsive/common/js/acc.checkoutaddress.js',
		// 'responsive/common/js/acc.checkoutsteps.js',
		// 'responsive/common/js/acc.cms.js',
		// 'responsive/common/js/acc.colorbox.js',
		'responsive/common/js/acc.common.js',
		'responsive/common/js/acc.faq.js',
		'responsive/common/js/acc.hopdebug.js',
		'responsive/common/js/acc.imagegallery.js',
		'responsive/common/js/acc.order.js',
		'responsive/common/js/acc.paginationsort.js',
		'responsive/common/js/acc.payment.js',
		// 'responsive/common/js/acc.paymentDetails.js',
		// 'responsive/common/js/acc.pickupinstore.js',
		'responsive/common/js/acc.product.js',
		'responsive/common/js/acc.productDetail.js',
		// 'responsive/common/js/acc.quickview.js',
		'responsive/common/js/acc.ratingstars.js',
		'responsive/common/js/acc.refinements.js',
		'responsive/common/js/acc.silentorderpost.js',
		'responsive/common/js/acc.tabs.js',
		'responsive/common/js/acc.track.js',
		'responsive/common/js/acc.trackorder.js',
		// 'responsive/common/js/acc.futurelink.js',
		'responsive/common/js/acc.productorderform.js',
		'responsive/common/js/acc.validate.js',
		'responsive/common/js/acc.voucher.js',
		'responsive/common/js/acc.productaddtocartcomponent.js',
		'responsive/common/js/acc.tabscroller.js',
		'responsive/common/js/acc.utils.js',
		'responsive/common/js/acc.placeorder.js',
		'responsive/common/js/acc.tagging.js',
		'responsive/common/js/acc.banner.js',
		'responsive/common/js/multistore/*.js',
		// 'responsive/common/js/_autoload.js',

		// 'responsive/common/js/cms/*.js',


		// 'addons/tokoadyenservices/responsive/common/js/common.js',
		
		// 'responsive/common/js/components/acc.productsList.js',
	];
	
	return {
		common : common_list,
	};
};