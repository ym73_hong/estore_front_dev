'use strict';

module.exports = (params) => {
	const path = require('path');
	const {src, dest, watch} = require('gulp');
	const gulp_less = require('gulp-less');
	const gulp_postcss = require('gulp-postcss');
	const autoprefixer = require('autoprefixer');
	const csswring = require('csswring');
	const gulp_sourcemaps = require('gulp-sourcemaps');
	const gulp_if = require('gulp-if');
	
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const src_base = `${params.webroot}/WEB-INF/_ui-src`;
	const src_build = [`${src_base}/responsive/themes/${params.site}/less/style.less`];
	const src_watch = [
		`${src_base}/shared/less/variableMapping.less`,
		`${src_base}/shared/less/generatedVariables.less`,
		`${src_base}/responsive/lib/ybase-*/less/**/*.less`,
		`${src_base}/responsive/lib/bootstrap-*/less/**/*.less`,
		`${src_base}/responsive/themes/${params.site}/less/**/*.less`
	];
	const dist = `${params.webroot}/_ui/responsive/theme-${params.site}/css`;
	
	const build = () => {
		const condition = () => params.mode === 'dev' ? true : false;
		
		return src(src_build ,options.src)
			.pipe(gulp_if(condition, gulp_sourcemaps.init()))
			.pipe(gulp_less())
			.pipe(gulp_postcss([
				autoprefixer(options.autoprefixer.less),
				csswring({removeAllComments: true}),
			]))
			.pipe(gulp_if(condition, gulp_sourcemaps.mapSources(function(sourcePath, file) {
				return path.join(path.relative(dist, file.base), sourcePath);
			})))
			.pipe(gulp_if(condition, gulp_sourcemaps.write('.', {includeContent: true})))
			.pipe(dest('.', {cwd: dist}))
			.on('end', () => {
				msg.success('LESS build');
			});
	};
 
	return {
		build: build,
		watch() {
			const watcher = watch(src_watch, options.watch, build);
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};