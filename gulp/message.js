'use strict';

module.exports = (() => {
	const gulp_messenger = require('gulp-messenger');
	
	// gulp_messenger 초기화
	gulp_messenger.init({
		logTimestampFormat: 'HH:mm:ss Z',
		timestamp: true,
		useDumpForObjects : true
	});

	return {
		log(event, path) {
			// let msg;
			// if(stats !== null && typeof stats === 'object') {
			// 	msg = `${path} : ${event}`;
			// } else {
			// 	msg = `${stats} : ${path}`;
			// }
			
			console.log(`\n${'='.repeat(80)}`);
			gulp_messenger.warning(`${path} : ${event}`);
		},
		
		success(path) {
			gulp_messenger.success(`${path} : Success !!`);
		},
		
		error(msg) {
			gulp_messenger.error(`${msg} : Fail`);
		},
		
		// error_plain: (msg) => {
		// 	gulp_messenger.log(`${msg}`);
		// }
	};
})();