'use strict';

// 모두 webpack 으로 전환하기 전까지만 사용 !!
module.exports = (params, withAngular) => {
	const {src, dest, watch} = require('gulp');
	const gulp_uglyfly = require('gulp-uglify');
	const gulp_concat = require('gulp-concat');
	const gulp_if = require('gulp-if');
	// const gulp_sourcemaps = require('gulp-sourcemaps');
	
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');
	const lib_list2 = require('./lib_list')(params.webroot, params.site, params.mode, withAngular); 
	
	const build = () => {
		const basePath = `${params.webroot}/_ui/responsive/theme-${params.site}/lib`;
		const distFile = withAngular ? 'lib_with_angular.js' : 'lib.js';
		const func_prd = () => src(lib_list2.getList(), options.src)
			// .pipe(gulp_sourcemaps.init())
			.pipe(gulp_if((file) => lib_list2.getIsMinified(file.path), gulp_uglyfly(options.uglyfly.lib_no_compress), gulp_uglyfly(options.uglyfly.lib)))
			.pipe(gulp_concat(distFile))
			// .pipe(gulp_sourcemaps.write('.', {includeContent: true, sourceRoot: '../src'}))
			.pipe(dest('dist', {cwd: basePath}))
			.on('end', () => {
				msg.success(`${distFile} build`);
			});
		const func_dev = () => src(lib_list2.getList(), options.src)
			// .pipe(gulp_sourcemaps.init())
			.pipe(gulp_concat(distFile, {newLine:';'}))
			// .pipe(gulp_uglyfly(options.uglyfly_app))
			// .pipe(gulp_sourcemaps.write('.', {includeContent: true, sourceRoot: '../src'}))
			.pipe(dest('dist', {cwd: basePath}))
			.on('end', () => {
				msg.success(`${distFile} build`);
			});
			
		return params.mode === 'dev' ? func_dev() : func_prd();
	};
 
	return {
		build: build,
		watch: () => {
			const watcher = watch(lib_list2.getList(), options.watch, build);
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
};
